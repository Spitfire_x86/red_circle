# red_circle
*(almost) completely useless Discord bot*

## List of Features

### Responding to ;v
**The main function of this bot.**

Responds with a Large Red Circle emoji: 🔴 to every message that contains this *cursed emoticon:* `;v` (also known as JaranEmote) or any variation thereof.

### Random Generators Galore

#### Age of Empires III Randomizer
**Command:** `v;aoe3 <no. of players> [civ list]`

Randomly picks civilizations for a given number of players from a specific AoE 3 civ list (from base game only if not specified) and a random map from the AoE 3 + Expansions map list.

Available civ lists:
- `base` - Basegame/European civs (default setting)
- `x` - The War Chiefs civs
- `y` - The Asian Dynasties civs
- `xy` - TWC and TAD civs
- `a` - All available (basegame and expansion pack) civs

#### Dice Roll
**Command:** `v;roll <n1>d<s1> [ <n2>d<s2> ...]`

  `n` - number of dice to roll

  `s` - number of sides on a die

Rolls the dice. Outputs the results of single rolls, sums of rolls in each group and a sum of everything.


#### More features will be added in the future.
