import discord
import random
srandom = random.SystemRandom()

# Command help. Will be moved to a separate file eventually.
help = [
    [
        'Help',
        '``v;roll`` - Rolls the dice. Accepts DnD format.'
    ], [
        'Usage',
        '''``v;roll <a1>d<b1> [<a2>d<b2>] ...``
        a1, a2 - amount of dice.
        b1, b2 - number of sides.
        Example: ``v;roll 2d6 3d4 1d10``'''
    ]
]


def collateOutput(pre=[], col=0x00ffff):
    '''Format any values as a discord embed.
    Returns formatted embed.

    Arguments:
    pre[] -- array of name-value pairs = [['name', value], ...]
    col -- embed color, defaults to cyan (#00FFFF)
    '''
    embed = discord.Embed(title='Dice Roll', description='', color=col)
    for field in pre:
        embed.add_field(name=field[0], value=field[1], inline=False)
    return embed


def rollDice(args=[]):
    '''Generate and output dice roll values, partial sums
    and a total sum of all rolls.
    Returns discord embed with generated values.
    If input args are incorrect, returns help instead.

    Arguments:
    args[] -- array passed from redcircle.py, should contain
              DnD dice notation - 'NdS', where:
              N -- number of dice to be rolled
              S -- number of faces on each die
              Example:
              ['2d6', '1d10', '4d4']
    '''
    if len(args) > 0:
        try:
            # Split args into an array -- [[n1, s1], [n2, s2], ...]
            dice = [[int(n) for n in a.split('d')] for a in args]
            results = []
            sums = []
            total = 0

            # Roll the dice
            for die in dice:
                if (die[0] > 100) or (die[0] < 0) or (die[1] > 100) or (die[1] < 0):
                    return collateOutput([['Error', 'Please enter values between 1 and 100.']], 0xff0000)
                roll = []
                sum = 0
                for i in range(1, die[0]+1):
                    n = srandom.randint(1, die[1])
                    roll.append(n)
                    sum += n
                    total += n
                results.append(roll)
                sums.append(sum)

            # Prepare the output for embed conversion
            preFormat = []
            for i, title in enumerate(args):
                content = str(results[i][0])
                for n in range(1, len(results[i])):
                    content += ' + ' + str(results[i][n])
                if len(results[i]) > 1:
                    content += ' = ' + str(sums[i])
                preFormat.append([title, content])
            if len(args) > 1:
                preFormat.append(['Total', total])

            return collateOutput(preFormat)

        except (ValueError, IndexError) as e:
            # Display error if args are not numbers
            return collateOutput([['Error', 'Invalid input.']], 0xff0000)

    else:
        # Display help
        return collateOutput(help, 0xff00ff)
    return
