import discord
import random
srandom = random.SystemRandom()

civs = {'aoe2': [], 'aoe3': []}
maps = {'aoe2': [], 'aoe3': []}
with open('aoe3civs.txt') as f:
    civs['aoe3'] = [line.rstrip('\n') for line in f]
with open('aoe3maps.txt') as f:
    maps['aoe3'] = [line.rstrip('\n') for line in f]

# Command help. Will be moved to a separate file eventually.
help3 = [
    [
        'Help',
        '``v;aoe3`` - Generates random Age of Empires 3  civilizations and maps.'
    ], [
        'Usage',
        '''``v;aoe3 <no_of_civs> [expansion_pack]``
        Example: ``v;aoe3 2 xy``'''
    ], [
        'Expansion packs',
        '''`base` - Base game civs
        `x` - War Chiefs civs
        `y` - Asian Dynasties civs
        `xy` - War Chiefs and Asian Dynasties civs
        `a` - All possible (basegame and expansion pack) civs
        Default value is `base`.'''
    ]
]

# Expansion pack list.
xps = ['base', 'x', 'y', 'xy', 'a']

# Civ list offsets.
# These number pairs are positions of first and last civ
# of a given expansion pack in the civ list.
xpCivs = {
    'aoe2': {},
    'aoe3': {
        'base': [0, 8],
        'x': [8, 11],
        'y': [11, 14],
        'xy': [8, 14],
        'a': [0, 14]
    }
}


def collateOutput(pre=[], col=0x00ff00):
    '''Format any values as a discord embed.
    Returns formatted embed.

    Arguments:
    pre[] -- array of name-value pairs = [['name', value], ...]
    col -- embed color, defaults to green (#00FF00)
    '''

    embed = discord.Embed(title='Age of Empires Randomizer', description='', color=col)
    for field in pre:
        embed.add_field(name=field[0], value=field[1], inline=False)
    return embed


def randomize(game, civNum, xp):
    '''Select and append random civs and map to schema.
    Returns schema = [['Civ 1', civ1], ['Civ2', civ2], ..., ['Map', map]].

    Arguments:
    game -- aoe3 or aoe2 (currently not implemented)
    civNum -- number of civs, max 8
    xp -- civ set/expansion pack name, must be in xps[]
    '''

    if civNum > 8:
        civNum = 8
    schema = []

    civRange = xpCivs[game][xp]
    for i in range(1, civNum+1):
        rCiv = srandom.choice(civs[game][civRange[0]:civRange[1]])
        schema.append(['Civ '+str(i), rCiv])

    schema.append(['Map', srandom.choice(maps[game])])
    return schema


def randAoe3(args=[]):
    '''Validate the input, display help and call randomize().
    Returns discord embed with values returned from randomize().
    If input args are incorrect, returns help instead.

    Arguments:
    args[] -- array passed from redcircle.py, should contain
              civ number and expansion pack (optional)
    '''

    if len(args) > 0 and args[0].isdigit():
        civNum = int(args[0])
        try:
            if args[1] in xps:
                xp = args[1]
            else:
                xp = xps[0]
        except IndexError:
            xp = xps[0]
        return collateOutput(randomize('aoe3', civNum, xp))
    else:
        return collateOutput(help3, 0xff00ff)
