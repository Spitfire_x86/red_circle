import discord
import asyncio
import re
import random
from diceRoll import rollDice
from aoeRandomizer import randAoe3

srandom = random.SystemRandom()

with open('aoe3civs.txt') as f:
    civs = [line.rstrip('\n') for line in f]
with open('aoe3maps.txt') as f:
    maps = [line.rstrip('\n') for line in f]
with open('token.txt') as f:
    token = f.readline().rstrip('\n')

jmoteF = re.compile(';[\b\W_]*[vV]')
jmoteB = re.compile('[vV][\b\W_]*;')

client = discord.Client()


@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')


@client.event
async def on_message(message):
    if message.content.startswith('v;aoe3'):
        args = message.content.split(' ')
        args.pop(0)
        await client.send_message(message.channel, embed=randAoe3(args))

    elif message.content.startswith('v;roll'):
        args = message.content.split(' ')
        args.pop(0)
        await client.send_message(message.channel, embed=rollDice(args))

    elif (jmoteF.search(message.content)) or (jmoteB.search(message.content)):
        await client.send_message(message.channel, message.author.mention+' :red_circle:')

client.run(token)
